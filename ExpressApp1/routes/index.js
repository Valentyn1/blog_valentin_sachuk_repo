'use strict';
const express = require('express');
const router = express.Router();
var activeGuestID = "0";
var ActPost = -1;
var LoginText = "", NUText="";

/* GET home page. */

class dataJson {
    constructor(filename) {
        this.fs = require('fs');                                     // ���������� ������ ������ �����
        this.filename = filename;
    };
    read() {
        let rdata = this.fs.readFileSync(this.filename, 'utf8');     // ������ ������ � ���������� rdata
        return JSON.parse(rdata);                                    // ����������� ��������� ���������� � �������
    };
    write(_data) {
        this.fs.writeFileSync(this.filename, JSON.stringify(_data));
    };
};

let data = new dataJson('./public/data/database.json');
let posts = data.read();                                            //������ ������ �� ���� ������ 
let gdata = new dataJson('./public/data/guests.json');
let guests = gdata.read();                                          //������ ������ �� ���� ��������

router.get('/', function (req, res) {
    var temptext = "Login";
    LoginText = "";
    res.render('index.ejs', { posts: posts, guests: guests, activeGuest: guests.find(item => item.ownerID == activeGuestID), temptext });
});

router.post('/', function (req, res) {
    res.setHeader('Content-Type', 'text/plain');                // �����, ������� ��������� ������ �� ����� 
    var i = req.body.deletePost;                                //�������� ����� ��������
    posts.splice(i, 1);                                         //������ ������� �� �������
    data.write(posts);                                          // ���������� ������� � ���� ������
    res.redirect('/');                                          // ������������ �� ������� ��������
}); 


router.get("/post/:id", function (req, res) {
    var id = req.params.id;
    var key = "disabled";
    ActPost = id - 1;
    if (posts[id - 1].ownerID == activeGuestID) { key = "" }; // ����������, �������� �� ������ �����, �������� ������, ���� ��, �� ���� ������ true, ���� ��� - false
    res.render('post.ejs', { post: posts[id - 1], guest: guests.find(item => item.ownerID == posts[id - 1].ownerID), key: key});
});

router.post('/post', function (req, res) {
    res.setHeader('Content-Type', 'text/plain');
    posts[ActPost].content = req.body.content;                //�������� ����� �������� � ������� ������
    posts[ActPost].title = req.body.title;
    data.write(posts);                                          // ���������� ������� � ���� ������
    res.redirect('/');                                          // ������������ �� ������� ��������
}); 

router.get('/write', function (req, res) {
    res.render('write.ejs', { posts: posts });
});

router.post('/write', function (req, res) {
    res.setHeader('Content-Type', 'text/plain');                // �����, ������� ��������� ������ �� ����� 
    var title = req.body.title;
    var content = req.body.content;
    var ownerID = activeGuestID;
    posts.push({ title: title, content: content, ownerID: ownerID });       //��������� ������ �� ����� � ������� ������
    data.write(posts);                                          // ���������� ������� � ���� ������
    res.redirect('/');                                          // ������������ �� ������� ��������
}); 

router.get('/regist', function (req, res) {
    NUText = "";
    res.render('login.ejs', { guests, activeGuestID, LoginText });
});

router.post('/regist', function (req, res) {
    var name = req.body.name;
    res.setHeader('Content-Type', 'text/plain');                // �����, ������� ��������� ������ �� �����
    var guestIndex = guests.findIndex(item => item.name == name);  
    if (guestIndex != -1) {
        if (guests[guestIndex].password == req.body.password) {
            activeGuestID = guests[guestIndex].ownerID;
            LoginText = "";
            res.redirect('/');
        }
         else {
            LoginText = "Login or password is incorrect";
            res.redirect('/regist');
            }
        }
        else {
            LoginText = "Login or password is incorrect";
            res.redirect('/regist');
        };
});

router.get('/addnewuser', function (req, res) {
    res.render('addnewuser.ejs', { NUText });
});

router.post('/addnewuser', function (req, res) {
    res.setHeader('Content-Type', 'text/plain');
    if (guests.findIndex(item => item.name == req.body.name) != -1) {
        NUText = "User allready exist";
        res.redirect('/addnewuser');
    } else
        if (req.body.password != req.body.password2) {
            NUText = "Passwords isn't equal";
            res.redirect('/addnewuser');
        } else {
            var name = req.body.name;
            var email = req.body.email;
            var password = req.body.password;
            var ownerID = String(guests.length);
            guests.push({ name: name, email: email, ownerID: ownerID, password: password });
            gdata.write(guests);
            activeGuestID = ownerID;
            NUText = "";
            res.redirect('/');
        }
});

router.get('/about', function (req, res) {
    res.render('about.ejs');
});

module.exports = router;
